/*
Druhý projekt do předmětu Operační systémy
Soubor: santa.c
Autor: David Spilka (xspilk00)
Datum vytvoření: 22.4.2013
Datum dokončení: 26.4.2013
Program řeší synchronizační problém známý pod pojmem Santa's Claus Problem
*/

//Knihovny
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <string.h>

//definice chybových kódů
#define PARAM_ERR 1
#define FOPEN_ERR 2
#define FORK_ERR 3
#define MEM_LOAD_ERR 4
#define SEM_CREATE_ERR 5
#define MEM_FREE_ERR 6
#define SEM_FREE_ERR 7

//definice hlášení santy
#define S_START 1
#define S_CHECK 2
#define S_CAN 3
#define S_FIN 4

//definice hlášení elfa
#define E_START 1
#define E_NEED 2
#define E_ASK 3
#define E_GOT_H 4
#define E_GOT_V 5
#define E_FIN 6

//Prototypy funkcí
void loadParams();
void printErr();
void loadMemory();
void loadSemaphores();
void freeMemory();
void freeSemaphores();
void santaMessage();
void elfMessage();
int randNumber();
void santaProc();
void elfProc();

//Globální proměnné
int *waiting = NULL, *active = NULL, *counter = NULL, *visit = NULL;
int shm_active_id = 0, shm_waiting_id = 0, shm_counter_id = 0;
sem_t *santa, *elf, *mutex, *anteroom, *writing, *vacation, *elf_start;
FILE *file = NULL;

//Struktura pro uložení parametrů programu
struct PARAMS {
    int elf_visit;
    int elf_count;
    int elf_time;
    int santa_time;
} params;

int main (int argc, char *argv[])
{
    loadParams(argc, argv); //Načtení parametrů

    //Otevření souboru, pokud se nezdaří vypiš chybu a ukonči se
    if((file = fopen("santa.out", "w")) == NULL)
    {
        printErr(FOPEN_ERR);
    }

    loadSemaphores();   //Načtení semaforů, při chybě vypiš hlášku a ukonči se
    loadMemory();       //Načtení sdílené paměti, při chybě uvolni zdroje a ukonči se

    //Vynulování proměnných sdílené paměti
    *(waiting) = 0;
    *(counter) = 0;
    *(active) = 0;

    //Vytvoření procesů, s ošetřením chyb
    int i;
    for( i = 0; i < params.elf_count + 1; i++ ) {
        switch(fork()) {
            case 0: /* child */
                if(i == 0)
                {
                    santaProc();    //spuštění procesu Santy
                }
                else
                {
                    elfProc(i);     //spuštění procesů elfů
                }
                break;
            case -1:
                printErr(FORK_ERR);     //chyba ve vytváření procesů
                break;
            default:  /* parent */
                break;
        }
    }

    //čekání na dokončení procesů potomků
    int status;
    for(i = 0; i < params.elf_count + 1; i++)
    {
        wait(&status);
    }

    freeMemory();       //uvolnění sdílené paměti
    freeSemaphores();   //uvolnění semaforů

    fclose(file);       //zavření soubor

    exit(0);            //konec
}

//Funkce vypisující chybová hlášení, uvolnění zdrojů a ukončení běhu programu
void printErr(int kod)
{
    switch(kod)
    {
        case PARAM_ERR:     //chyba v zadání parametrů
            fprintf(stderr, "Chybne zadane parametry programu\n");
            exit(1);
        break;
        case FOPEN_ERR:     //chyba v otevření souboru
            fprintf(stderr, "Nepodařilo se otevřít/vytvořit soubor santa.out, program bude ukončen!\n");
            exit(2);
        break;
        case FORK_ERR:      //chyba ve vytváření procesů
            fprintf(stderr, "Nepodařilo se vytvořit procesy, program bude ukončen\n");
            freeMemory();
            freeSemaphores();
            fclose(file);
            exit(2);
        break;
        case MEM_LOAD_ERR:  //chyba v alokaci sdílené paměti
            fprintf(stderr, "Nepodařilo se naalokovat paměť, program bude ukončen\n");
            freeMemory();
            fclose(file);
            exit(2);
        break;
        case SEM_CREATE_ERR:    //chyba při vytváření semforů
            fprintf(stderr, "Nepodařilo se vytvořit semafory, program bude ukončen\n");
            freeMemory();
            freeSemaphores();
            fclose(file);
            exit(2);
        break;
        case MEM_FREE_ERR:      //chyba při uvolňování sdílené paměti
            fprintf(stderr, "Nepodařilo se uvolnit naalokovanou paměť\n");
        break;
        case SEM_FREE_ERR:      //chyba pro uvolňování semaforů
            fprintf(stderr, "Nepodařilo zničit semafory\n");
        break;
        default:
            fprintf(stderr, "Nespecifikovaná chyba všechno špatně\n");
            exit(2);
        break;
    }
    return;
}

void santaProc(void)
{
    santaMessage(S_START);  //Santa stratuje
    while(1)
    {
        sem_wait(santa);    //santa čeká
        if(*active == 0)    //pokud je aktivních skřítků 0
        {
            santaMessage(S_CHECK);      //vypíše check...
            sem_post(vacation);         //pustí skřítky z dovolené, aby mohli umřít
            santaMessage(S_FIN);        //vypíše, že končí
            exit(0);                    //a ukončí se
        }
        santaMessage(S_CHECK);          //vypíše check...
        santaMessage(S_CAN);            //vypíš can help
        usleep(randNumber(params.santa_time));      //pomáhá skřítkům
        sem_wait(mutex);
        //vypustí skřítky
        if(*waiting == 3)
        {
            sem_post(elf);
        }
        else if(*waiting == 1)
        {
            sem_post(elf);
        }
        sem_post(mutex);
    }
}

void elfProc(int elf_id)
{
    int visit = 0;  //nastavení počtu náštěv u santy na 0
    sem_wait(mutex);
    elfMessage(E_START, elf_id);        //vypíše, že startuje
    sem_post(mutex);
    //čekání než se nastartujou všichni skřítci
    sem_wait(mutex);
    if(*active == params.elf_count)
    {
            sem_post(elf_start);
    }
    sem_post(mutex);

    sem_wait(elf_start);
    sem_post(elf_start);

    while(visit < params.elf_visit)     //dokud počet návštěv skřítka nedosáhne maxima návštěv
    {
        usleep(randNumber(params.elf_time));        //skřítek pracuje
        sem_wait(mutex);
        elfMessage(E_NEED, elf_id);                 //vypíše, že potřebuje pomoct
        sem_post(mutex);
        sem_wait(anteroom);                         //zamkne čekárnu
        sem_wait(mutex);
        elfMessage(E_ASK, elf_id);                  //zeptá se o pomoc
        sem_post(mutex);
        sem_wait(mutex);
        if(*active > 3 && *waiting < 3)             //pokud ještě v čekárně nejsou 3 a aktivních je jich víc jak 3, tak ji odemkne
        {
            sem_post(anteroom);
        }
        else if(*active <= 3 && *waiting == 1)      //pokud je aktivních míň jak 3 a je sám v čekárně, tak zbudí santu
        {
            sem_post(santa);
        }
        else if(*active > 3 && *waiting == 3)       //pokud je aktivních víc jak 3 a již 3 čekají, tak probudí santu
        {
            sem_post(santa);
        }
        sem_post(mutex);
        sem_wait(elf);              //a uspí se
        sem_wait(mutex);
        elfMessage(E_GOT_H, elf_id);        //vypíše, že dostal pomoc, poté mu santa pomůže
        sem_post(elf);
        visit++;                            //zvýší si počítadlo návštěv
        if(visit == params.elf_visit)       //a pokud jeho počítadlo dosáhlo magima návštěv
            elfMessage(E_GOT_V, elf_id);    //vypíše, že dostal dovolenou a vyskočí z cyklu
        if(*waiting == 0){                  //pokud je již čekajících skřítků 0, tak odemkne čekárnu
            sem_wait(elf);
            sem_post(anteroom);
        }
        sem_post(mutex);
    }

    sem_wait(mutex);
    if(*active == 0)                    //pokud je již aktivních skřítků 0. tak se odemke santa, aby odemkl dovolené a ukončil se
    {
        sem_post(santa);
    }
    sem_post(mutex);

    sem_wait(vacation);                 //zde jsou skřítci, kteří byli posláni na dovolenou
    sem_post(vacation);                 //po jednom odcházejí umřít
    elfMessage(E_FIN, elf_id);          //vypíše, že končí
    exit(0);                            //a ukončí se
}

//Funkce pro načtení a zpracování parametrů, při chybě volá funkci pro výpis chybové hlášky
void loadParams(int argc, char *argv[])
{
    if(argc == 5)
    {
        char *pEnd;

        if((params.elf_visit = strtol(argv[1], &pEnd, 10)) <= 0)
        {
            printErr(PARAM_ERR);
            return;
        }
        if((params.elf_count = strtol(argv[2], &pEnd, 10)) <= 0)
        {
            printErr(PARAM_ERR);
            return;
        }
        params.elf_time = strtol(argv[3], &pEnd, 10);
        if(pEnd == argv[3] || *pEnd != '\0' || params.elf_time < 0)
        {
            printErr(PARAM_ERR);
            return;
        }
        params.santa_time = strtol(argv[4], &pEnd, 10);
        if(pEnd == argv[4] || *pEnd != '\0' || params.santa_time < 0)
        {
            printErr(PARAM_ERR);
            return;
        }
    }
    else
    {
        printErr(PARAM_ERR);
        return;
    }

    return;
}

//Funkce pro načtení sdílené paměti, při chybě volá funkci pro výpis chybové hlášky
void loadMemory( void )
{
    int err = 0;
    if((shm_active_id = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | IPC_EXCL | 0666)) == -1)
    { err = 1; }
    if((active = (int *) shmat(shm_active_id, NULL, 0)) == NULL)
    { err = 1; }
    if((shm_waiting_id = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | IPC_EXCL | 0666)) == -1)
    { err = 1; }
    if((waiting = (int *) shmat(shm_waiting_id, NULL, 0)) == NULL)
    { err = 1; }
    if((shm_counter_id = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | IPC_EXCL | 0666)) == -1)
    { err = 1; }
    if((counter = (int *) shmat(shm_counter_id, NULL, 0)) == NULL)
    { err = 1; }

    if(err == 1)
        printErr(MEM_LOAD_ERR);
}

//Funkce pro načtení semaforů, při chybě volá funkci pro výpis chybové hlášky
void loadSemaphores( void )
{
    int err = 0;
    if((santa = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0)) == MAP_FAILED)
    { err = 1; }
    if((elf = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0)) == MAP_FAILED)
    { err = 1; }
    if((mutex = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0)) == MAP_FAILED)
    { err = 1; }
    if((anteroom = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0)) == MAP_FAILED)
    { err = 1; }
    if((writing = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0)) == MAP_FAILED)
    { err = 1; }
    if((vacation = mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0)) == MAP_FAILED)
    { err = 1; }
    if((elf_start= mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, 0, 0)) == MAP_FAILED)
    { err = 1; }

    if(sem_init(santa, 1, 0) == -1)
    { err = 1; }
    if(sem_init(elf, 1, 0) == -1)
    { err = 1; }
    if(sem_init(mutex, 1, 1) == -1)
    { err = 1; }
    if(sem_init(anteroom, 1, 1) == -1)
    { err = 1; }
    if(sem_init(writing, 1, 1) == -1)
    { err = 1; }
    if(sem_init(vacation, 1, 0) == -1)
    { err = 1; }
    if(sem_init(elf_start, 1, 0) == -1)
    { err = 1; }

    if(err == 1)
        printErr(SEM_CREATE_ERR);
}

//Funkce pro uvolnění paměti, při chybě volá funkci pro výpis chybové hlášky
void freeMemory( void )
{
    int err = 0;
    if(shmdt(active) == -1)
    { err = 1; }
    if(shmdt(waiting) == -1)
    { err = 1; }
    if(shmdt(counter) == -1)
    { err = 1; }
    if(shmctl(shm_active_id, IPC_RMID, NULL) == -1)
    { err = 1; }
    if(shmctl(shm_waiting_id, IPC_RMID, NULL) == -1)
    { err = 1; }
    if(shmctl(shm_counter_id, IPC_RMID, NULL) == -1)
    { err = 1; }

    if(err == 1)
        printErr(MEM_FREE_ERR);
}

//Funkce pro uvolnění semaforů, při chybě volá funkci pro výpis chybové hlášky
void freeSemaphores( void )
{
    int err = 0;
    if(sem_destroy(santa) == -1)
    { err = 1; }
    if(sem_destroy(elf) == -1)
    { err = 1; }
    if(sem_destroy(mutex) == -1)
    { err = 1; }
    if(sem_destroy(anteroom) == -1)
    { err = 1; }
    if(sem_destroy(writing) == -1)
    { err = 1; }
    if(sem_destroy(vacation) == -1)
    { err = 1; }
    if(sem_destroy(elf_start) == -1)
    { err = 1; }

    if(err == 1)
        printErr(SEM_FREE_ERR);
}

//Funkce pro výpis zpráv santy, zaimplementováno i počítadlo akcí
void santaMessage(int msg)
{
    sem_wait(writing);
    (*counter)++;
    switch(msg)
    {
        case S_START:
            fprintf(file, "%d: santa: started\n", (*counter));
            fflush(NULL);
        break;

        case S_CHECK:
            fprintf(file, "%d: santa: checked state: %d: %d\n", (*counter), (*active), (*waiting));
            fflush(NULL);
        break;

        case S_CAN:
            fprintf(file, "%d: santa: can help\n", (*counter));
            fflush(NULL);
        break;

        case S_FIN:
            fprintf(file, "%d: santa: finished\n", (*counter));
            fflush(NULL);
        break;
    }
    //fflush(NULL);
    sem_post(writing);
}

//Funkce pro výpis zpráv elfa, zaimplementováno i počítadlo akcí, navyšování a snižování čekajících a aktivních skřítků
void elfMessage(int msg, int elf)
{
    sem_wait(writing);
    (*counter)++;
    switch(msg)
    {
        case E_START:
            (*active)++;
            fprintf(file, "%d: elf: %d: started\n", (*counter), elf);
            fflush(NULL);
        break;

        case E_NEED:
            fprintf(file, "%d: elf: %d: needed help\n", (*counter), elf);
            fflush(NULL);
        break;

        case E_ASK:
            (*waiting)++;
            fprintf(file, "%d: elf: %d: asked for help\n", (*counter), elf);
            fflush(NULL);
        break;

        case E_GOT_H:
            (*waiting)--;
            fprintf(file, "%d: elf: %d: got help\n", (*counter), elf);
            fflush(NULL);
        break;

        case E_GOT_V:
            (*active)--;
            fprintf(file, "%d: elf: %d: got a vacation\n", (*counter), elf);
            fflush(NULL);
        break;

        case E_FIN:
            fprintf(file, "%d: elf: %d: finished\n", (*counter), elf);
            fflush(NULL);
        break;
    }
    sem_post(writing);
}

//Funkce vracející náhodné číslo z intervali <0,cas>
int randNumber(int cas)
{
    if(cas == 0)
    {
        return 0;
    }
    cas++;
    srand(time(NULL));
    int nah = ((rand()+(*counter)) % cas)*1000;
    return nah;
}
