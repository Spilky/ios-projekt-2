# Druhý projekt do předmětu Operační systémy
# Soubor: Makefile
# Autor: David Spilka (xspilk00)
# Datum vytvoření: 22.4.2013
# Datum dokončení: 26.4.2013
# Program řeší synchronizační problém známý pod pojmem Santa's Claus Problem

all: santa

santa: santa.c
	gcc -std=gnu99 -Wall -Wextra -Werror -pedantic -pthread -o santa santa.c
